package solomia;

import jdk.internal.org.xml.sax.SAXException;
import solomia.controller.ParserControllerImpl;
import solomia.model.Sweet;
import solomia.parser.SAX.SaxParser;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class  Application{
    public static void main(String[] args) {
        new ParserControllerImpl().runSaxParser();


//        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
//        try {
//            SAXParser saxParser = saxParserFactory.newSAXParser();
//            SaxParser handler = new SaxParser();
//            saxParser.parse(new File("src\\main\\resources\\XML\\examples\\SweetXML.xml"), handler);
//            //Get Employees list
//            List<Sweet> candies = handler.getCandyList();
//            //print employee information
//            for(Sweet emp : candies)
//                System.out.println(emp);
//        } catch (ParserConfigurationException | IOException e) {
//            e.printStackTrace();
//        } catch (org.xml.sax.SAXException e) {
//            e.printStackTrace();
//        }
    }
}
