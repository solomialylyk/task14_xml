package solomia.controller;

public interface ParserController {
    void runDOMParser();

    void runSaxParser();

    void runStaxParser();

}
