package solomia.controller;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import solomia.model.Sweet;
import solomia.parser.SAX.SaxParser;
import solomia.parser.XmlValidator;
import solomia.parser.dom.parseDOM;
import solomia.parser.stax.StaxParser;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class ParserControllerImpl implements ParserController {
    //private static Logger log = LogManager.getLogger(ParserControllerImpl.class);
    private parseDOM domParser;
    private SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    //private CandyComparator comparator;
    private File xmlFile = new File("src\\main\\resources\\XML\\examples\\SweetXML.xml");
    private File xsdFile = new File("src\\main\\resources\\XML\\examples\\SweetXSD.xsd");

    public ParserControllerImpl() {
        domParser = new parseDOM();
        //comparator = new CandyComparator();
    }

    @Override
    public void runDOMParser() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
            System.out.println("DOM");
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | IOException e) {
            //log.error(e.getMessage());
            System.out.println("error");
        } catch (SAXException e) {
            e.printStackTrace();
        }
        if (XmlValidator.validate(document, xsdFile)) {
            List<Sweet> candies = domParser.parseDOM(document);
            //candies.sort(comparator);
            //log.info(candies);
            System.out.println(candies);
        } else {
            //log.error("XML document failed validation.");
            System.out.println("error validation");
        }
    }

    @Override
    public void runSaxParser() {
        List<Sweet> candies = new ArrayList<>();
        try {
            System.out.println("SAX");
            saxParserFactory.setSchema(XmlValidator.createSchema(xsdFile));
            saxParserFactory.setValidating(true);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxParser saxHandler = new SaxParser();
            saxParser.parse(xmlFile, saxHandler);
            candies = saxHandler.getCandyList();
            //candies.sort(comparator);
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            System.out.println("error");
        }
        System.out.println(candies);
    }


    @Override
    public void runStaxParser() {
        StaxParser staxParser = new StaxParser();
        System.out.println("......STAX.....");
        List<Sweet> candies = staxParser.parseStax(xmlFile);
        //candies.sort(comparator);
        //log.info(candies);
        System.out.println(candies);
    }

}
