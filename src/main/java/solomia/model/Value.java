package solomia.model;

public class Value {
    private int proteins;
    private int carbohydrates;
    private int fats;

    public Value() {
    }

    public Value(int proteins, int carbohydrates, int fats) {
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
    }

    public int getProteins() {
        return proteins;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public int getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public int getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    @Override
    public String toString() {
        return "Value{" +
                "proteins='" + proteins + '\'' +
                ", carbohydrates='" + carbohydrates + '\'' +
                ", fats='" + fats + '\'' +
                '}';
    }
}
