package solomia.parser.SAX;

import jdk.internal.org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import solomia.model.Ingredient;
import solomia.model.Sweet;
import solomia.model.Value;

import java.util.ArrayList;
import java.util.List;


public class SaxParser extends DefaultHandler {
    private List<Sweet> candyList;
    private Sweet candy;
    private Value value;
    private List<Ingredient> ingredients;
    private String currentElement;

    public SaxParser() {
        candyList = new ArrayList<>();
    }

    public List<Sweet> getCandyList() {
        return this.candyList;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = qName;
        switch (currentElement) {
            case "sweet":
                candy = new Sweet();
                candy.setNum(Integer.parseInt(attributes.getValue("num")));
                break;
            case "value":
                value = new Value();
                break;
            case "ingredient":
                ingredients = new ArrayList<>();
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "sweet":
                candyList.add(candy);
                break;
            case "value":
                candy.setValue(value);
                value = new Value();
                break;
            case "ingredient":
                candy.setIngredients(ingredients);
                ingredients = new ArrayList<>();
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        switch (currentElement) {
            case "name":
                candy.setName(new String(ch, start, length));
                break;
            case "energy":
                candy.setEnergy(Integer.parseInt(new String(ch, start, length)));

            case "type":
                candy.setType(new String(ch, start, length));
                break;
            case "production":
                candy.setProduction(new String(ch, start, length));
                break;
        }
        parseValueFields(ch, start, length);
        parseIngredientsList(ch, start, length);

    }

    private void parseValueFields(char[] ch, int start, int length) {
        switch (currentElement) {
            case "proteins":
                value.setProteins(Integer.parseInt(new String(ch, start, length)));
                break;
            case "fats":
                value.setFats(Integer.parseInt(new String(ch, start, length)));
                break;
            case "carbohydrates":
                value.setCarbohydrates(Integer.parseInt(new String(ch, start, length)));
                break;
        }
    }

    private void parseIngredientsList(char[] ch, int start, int length) {
        if (currentElement.equals("ingredients")) {
            Ingredient ingredient = new Ingredient();
            ingredient.setName(new String(ch, start, length));
            ingredients.add(ingredient);
        }
    }
}
