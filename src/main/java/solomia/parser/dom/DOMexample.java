package solomia.parser.dom;

import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import solomia.model.Sweet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
@Deprecated
public class DOMexample {
    private static List<Sweet> sweets = new ArrayList<>();

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, org.xml.sax.SAXException {
        // Получение фабрики, чтобы после получить билдер документов.
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // Получили из фабрики билдер, который парсит XML, создает структуру Document в виде иерархического дерева.
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Запарсили XML, создав структуру Document. Теперь у нас есть доступ ко всем элементам, каким нам нужно.
        Document document = builder.parse(new File("D:\\IntelijIdea\\task14_XML\\src\\main\\resources\\XML\\examples\\SweetXML.xml"));
        // Получение списка всех элементов employee внутри корневого элемента (getDocumentElement возвращает ROOT элемент XML файла).
        NodeList sweetElements = document.getDocumentElement().getElementsByTagName("employee");

        // Перебор всех элементов employee
        for (int i = 0; i < sweetElements.getLength(); i++) {
            Node sweet = sweetElements.item(i);
            // Получение атрибутов каждого элемента
            NamedNodeMap attributes = sweet.getAttributes();

            // Добавление сотрудника. Атрибут - тоже Node, потому нам нужно получить значение атрибута с помощью метода getNodeValue()
            sweets.add(new Sweet(attributes.getNamedItem("name").getNodeValue()));
        }

        // Вывод информации о каждом сотруднике
        for (Sweet sweet : sweets)
            System.out.println(String.format(sweet.getName(), sweet.getEnergy()));
    }
}

