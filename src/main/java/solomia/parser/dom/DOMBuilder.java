package solomia.parser.dom;

import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import solomia.model.Sweet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public class DOMBuilder {
        private Set<Sweet> sweets;
        private DocumentBuilder docBuilder;
        public DOMBuilder() {
            this.sweets = new HashSet<Sweet>();
// создание DOM-анализатора
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            try {
                docBuilder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                System.err.println("Ошибка конфигурации парсера: " + e);
            }
        }
        public Set<Sweet> getStudents() {
            return sweets;
        }
        public void buildSetStudents(String fileName) {
            Document doc = null;
            try {
// parsing XML-документа и создание древовидной структуры
                doc = docBuilder.parse(fileName);
                Element root = doc.getDocumentElement();
// получение списка дочерних элементов <student>
                NodeList sweetsList = root.getElementsByTagName("sweet");
                for (int i = 0; i < sweetsList.getLength(); i++) {
                    Element sweetElement = (Element) sweetsList.item(i);
                    //Sweet sweet = buildSweet(sweetElement);
                    //sweets.add(sweet);
                }
            } catch (IOException e) {
                System.err.println("File error or I/O error: " + e);
            } catch (org.xml.sax.SAXException e) {
                e.printStackTrace();
            }
        }
//        private Sweet buildSweet(Element studentElement) {
//            Sweet sweet = new Sweet();
//// заполнение объекта student
//            sweet.setName(getElementTextContent(studentElement, "name"));
//            Integer tel = Integer.parseInt(getElementTextContent(
//                    studentElement,"telephone"));
//            sweet.setTelephone(tel);
//            Sweet.Address address = sweet.getAddress();
//// заполнение объекта address
//            Element adressElement = (Element) studentElement.getElementsByTagName("address").item(0);
//            address.setCountry(getElementTextContent(adressElement, "country"));
//            address.setCity(getElementTextContent(adressElement, "city"));
//            address.setStreet(getElementTextContent(adressElement, "street"));
//            sweet.setLogin(studentElement.getAttribute("login"));
//            return sweet;
//        }
        // получение текстового содержимого тега
        private static String getElementTextContent(Element element, String elementName) {
            NodeList nList = element.getElementsByTagName(elementName);
            Node node = nList.item(0);
            String text = node.getTextContent();
            return text;
        }

}
