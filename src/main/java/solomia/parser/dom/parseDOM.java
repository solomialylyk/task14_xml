package solomia.parser.dom;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import solomia.model.Ingredient;
import solomia.model.Sweet;
import solomia.model.Value;

import java.util.ArrayList;
import java.util.List;

public class parseDOM {

    public List<Sweet> parseDOM(Document document) {
        List<Sweet> candies = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("sweet");
        for (int index = 0; index < nodeList.getLength(); index++) {
            Sweet candy = new Sweet();
            Value value;
            Node node = nodeList.item(index);
            Element element = (Element) node;
            value = getValue(element.getElementsByTagName("value"));
            setRootElements(element, candy);
            candy.setIngredients(getIngredient(element.getElementsByTagName("ingredient")));
            candy.setValue(value);
            candies.add(candy);
        }
        return candies;
    }

    private void setRootElements(Element element, Sweet candy) {
        candy.setNum(Integer.parseInt(element.getAttribute("num")));
        candy.setName(element.getElementsByTagName("name").item(0).getTextContent());
        candy.setType(element.getElementsByTagName("type").item(0).getTextContent());
        candy.setEnergy(Integer.parseInt(element.getElementsByTagName("energy").item(0)
                .getTextContent()));
        candy.setProduction(element.getElementsByTagName("production").item(0).getTextContent());
    }

    private Value getValue(NodeList nodeList) {
        Value value = new Value();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            value.setFats(Integer.parseInt(element.getElementsByTagName("fats")
                    .item(0).getTextContent()));
            value.setCarbohydrates(Integer.parseInt(element.getElementsByTagName("carbohydrates")
                    .item(0).getTextContent()));
            value.setProteins(Integer.parseInt(element.getElementsByTagName("proteins")
                    .item(0).getTextContent()));
        }
        return value;
    }

    private List<Ingredient> getIngredient(NodeList nodeList) {
        List<Ingredient> ingredients = new ArrayList<>();
        for (int index = 0; index < nodeList.getLength(); index++) {
            Ingredient ingredient = new Ingredient();
            Node node = nodeList.item(index);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                ingredient.setName(element.getTextContent());
                ingredients.add(ingredient);
            }
        }
        return ingredients;
    }
}