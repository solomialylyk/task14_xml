package solomia.parser.stax;

import solomia.model.Ingredient;
import solomia.model.Sweet;
import solomia.model.Value;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {
    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    private List<Sweet> candies = new ArrayList<>();
    private Sweet candy;
    private Value value;
    private List<Ingredient> ingredients;

    public List<Sweet> parseStax(File xml) {
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    xmlEvent = xmlEventReader.nextEvent();
                    getRootElement(name, startElement, xmlEvent);
                    getValueElement(name, xmlEvent);
                    getIngredientList(name, xmlEvent);
                }
                endElement(xmlEvent);
            }
        } catch (FileNotFoundException | XMLStreamException el) {
            //log.error(el.getMessage());
        }
        return candies;
    }

    private void getValueElement(String name, XMLEvent xmlEvent) {
        switch (name) {
            case "value":
                value = new Value();
                break;
            case "proteins":
                value.setProteins(Integer.parseInt(xmlEvent.asCharacters().getData()));
                break;
            case "fats":
                value.setFats(Integer.parseInt(xmlEvent.asCharacters().getData()));
                break;
            case "carbohydrates":
                value.setCarbohydrates(Integer.parseInt(xmlEvent.asCharacters().getData()));
                break;
        }
    }

    private void getIngredientList(String name, XMLEvent xmlEvent) {
        switch (name) {
            case "ingredients":
                ingredients = new ArrayList<>();
                break;
            case "name":
                Ingredient ingredient = new Ingredient();
                ingredient.setName(xmlEvent.asCharacters().getData());
                ingredients.add(ingredient);
                break;
        }
    }

    private void getRootElement(String name, StartElement startElement, XMLEvent xmlEvent) {
        switch (name) {
            case "sweet":
                candy = new Sweet();
                Attribute candyId = startElement.getAttributeByName(
                        new QName("num"));
                if (candyId != null) {
                    candy.setNum(Integer.parseInt(candyId.getValue()));
                }
                break;
            case "name":
                candy.setName(xmlEvent.asCharacters().getData());
                break;
            case "energy":
                candy.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                break;
            case "production":
                candy.setProduction(xmlEvent.asCharacters().getData());
                break;
        }
    }

    private void endElement(XMLEvent xmlEvent) {
        if (xmlEvent.isEndElement()) {
            EndElement endElement = xmlEvent.asEndElement();
            String name = endElement.getName().getLocalPart();
            switch (name) {
                case "sweet": {
                    candies.add(candy);
                    candy = null;
                    break;
                }
                case "value": {
                    candy.setValue(value);
                    value = null;
                    break;
                }
                case "ingredients": {
                    candy.setIngredients(ingredients);
                    ingredients = null;
                    break;
                }
            }
        }
    }
}
